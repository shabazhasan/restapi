import re
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User



USERNAME_MAX_LENGTH = 12
USERNAME_MIN_LENGTH = 6

class UserRegistrationSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=USERNAME_MAX_LENGTH,min_length=USERNAME_MIN_LENGTH)
    email = serializers.CharField()
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)


    def validate_email(self,email):
        email_pattern = re.compile('^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$')
        valid_email = email_pattern.match(email)
        email_exist = User.objects.filter(email=email).exists()

        if email_exist:
            raise serializers.ValidationError("Email already exists")

        if not valid_email:
           raise serializers.ValidationError("Not a valid email")

        return email


    def validate_username(self,username):
        user_exist = User.objects.filter(username=username).exists()

        if user_exist:
            raise serializers.ValidationError("Username already exists")

        return username


    def validate(self,data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError("Passwords did not match")

        return data


    def create(self, validated_data):
        validated_data['password'] = validated_data.pop('password1')
        validated_data.pop('password2')

        user = User(**validated_data)
        if not user:
            raise serializers.ValidationError("User is not created with given credentials")
        user.save()

        token, created = Token.objects.get_or_create(user=user)
        return user


class LoginSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=USERNAME_MAX_LENGTH,min_length=USERNAME_MIN_LENGTH)
    password = serializers.CharField(write_only=True)

    def create(self,validated_data):
        username = validated_data['username']
        password = validated_data['password']
        try:
            user = User.objects.get(username=username,password=password)
        except User.DoesNotExist:
            raise serializers.ValidationError("wrong username or password")

        try:
            token = Token.objects.get(user=user)
            raise serializers.ValidationError("User already logged in")
        except Token.DoesNotExist:
            token = Token.objects.create(user=user)

        return user

class LogoutSerializer(serializers.Serializer):

    token =serializers.CharField(write_only=True)

    def create(self, validated_data):
        try:
            token = Token.objects.get(key=validated_data['token'])
        except Token.DoesNotExist:
            raise serializers.ValidationError("Token does not exist")
        token.delete()
        return token 
