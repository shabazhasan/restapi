from django.conf.urls import url

from .api.v1.views import RegisterView,LoginView,LogoutView

urlpatterns = [
    url(r'^register/$', RegisterView.as_view(),name="rest_register"),
    url(r'^login/$', LoginView.as_view(),name="login"),
    url(r'^logout/$', LogoutView.as_view(),name="logout")
]
